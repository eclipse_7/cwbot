import re
from client.functions.regexp import PROFILE, HERO


def parse_profile(text):
    parsed_data = re.search(PROFILE, text)
    if parsed_data:
        bot_profile = {'castle': str(parsed_data.group(1)), 'name': str(parsed_data.group(2)),
                       'prof': str(parsed_data.group(3)), 'level': int(parsed_data.group(4)),
                       'attack': int(parsed_data.group(5)), 'defence': int(parsed_data.group(6)),
                       'exp': int(parsed_data.group(7)), 'needExp': int(parsed_data.group(8)),
                       'maxStamina': int(parsed_data.group(10)), 'gold': int(parsed_data.group(11)),
                       'donateGold': int(parsed_data.group(12))}
        return bot_profile
    else:
        return None


def parse_hero(text):
    parsed_data = re.search(HERO, text)
    if parsed_data:
        bot_hero = {'castle': str(parsed_data.group(1))}
        return bot_hero
    else:
        return None