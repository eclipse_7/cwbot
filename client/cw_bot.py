import asyncio
import configparser
import re
import time
import traceback

from telethon import TelegramClient, events
from telethon.tl.types import Channel

config = configparser.ConfigParser()
config.read('config.ini')
SQUAD_SUPERGROUP_ID = int(config.get('CORE', 'SQUAD_SUPERGROUP_ID'))

TELEGRAM_USER_ID = 777000

CASTLES = ('🍆', '🖤', '🦇', '🐢', '🍁', '☘', '🌹')

class Quest:
    def __init__(self, name, text=None, enable=False, intensity=2.0, interval=310, start_time=7, end_time=24):
        self.name = name
        self.text = text
        self.enable = enable
        self.intensity = intensity
        self.interval = interval
        self.last_time = 0
        self.start_time = start_time
        self.end_time = end_time


class CWGameBot:
    """Chat Wars Game Bot"""
    def __init__(self, session_name, user_phone, api_id, api_hash):
        # super().__init__(session_user_id, api_id, api_hash)

        self.__client = None
        self.__session = session_name
        self.__api_id = api_id
        self.__api_hash = api_hash

        self._subclient = None

        self.TARGETS = {'🇪🇺': '🇪🇺', '🇮🇲': '🇮🇲',
                        '🇲🇴': '🇲🇴', '🇬🇵': '🇬🇵',
                        '🇻🇦': '🇻🇦', '🇨🇾': '🇨🇾',
                        '🇰🇮': '🇰🇮', '⛰': '⛰Горный форт',
                        '🌲': '🌲Лесной форт', '⚓': '⚓Морской форт'}

        self.ATTACK_ACTION = '⚔Атака'
        self.REPORTS_TIME = (8, 12, 16, 20, 24)

        self.found_media = set()

        self.user_phone = user_phone
        self.username = None
        self.authorized = False

        # thread
        self._thread_running = True
        self._thread = None

        self._castle = None

        # entities
        self._game_entity = None
        self._squad_entity = None
        self._build_report_entity = None

        self._market_bot = None
        self._trade_run = False
        self._trade_with_username = None
        self._trade_bot_entity = None

        self._caravan_catch = False
        self._halt = False
        self._bath = False
        self._sleep_to = 0

        self._forward_to_squad = False

        # quests
        self.QUESTS_LIST = ('forest', 'caravan', 'cave', 'coast', 'build')
        self.QUESTS_LEN = len(self.QUESTS_LIST)
        self.quests_dict = {'forest': Quest('forest', '🌲Лес', intensity=1.7),
                            'caravan': Quest('caravan', '🐫ГРАБИТЬ КОРОВАНЫ', intensity=0.5),
                            'cave': Quest('cave', '🕸Пещера', intensity=0.5),
                            'coast': Quest('coast', '🏝Побережье', intensity=1.7),
                            'build': Quest('build', intensity=10)}

        self.quest_selector = 0
        self.time_of_last_message_quest = 0

        # battle
        self.auto_donate = False
        self.auto_def = False

        # level up
        self.level_up = False
        self.level_up_msg = None

        # reports
        self.auto_report = False
        self.auto_build_report = False

        # squad & castle core id's
        self.castle_bot_id = None

        # utils
        self._last_attack_target = ''
        self._attack_target = ''

    def get_username(self):
        return self.username

    async def _get_and_forward_async(self, text):
        await self.__client.send_message(self._game_entity, text)
        self._forward_to_squad = True

    # async def _attack(self, target):
    #     if self._halt or (not target):
    #         return
    #     t = self.TARGETS.get(target)
    #     if t:
    #         self.send_message(self._game_entity, self.ATTACK_ACTION, link_preview=False)
    #         await asyncio.sleep(1.0)
    #         message = self.get_message_history(self._game_entity, limit=1)[0]
    #         if message and message.message.startswith('Смелый вояка'):
    #             await self._get_and_forward_async(t)

    async def _status(self):
        forest_text = 'on' if self.quests_dict['forest'].enable else 'off'
        caravan_text = 'on' if self.quests_dict['caravan'].enable else 'off'
        caravan_catch_text = 'on' if self._caravan_catch else 'off'

        # text = '\n'.join([
        #     'Замок: {}',
        #     '🌲 Лес: {}',
        #     '🛡 🐫: {}',
        #     '🗡 🐫: {}'
        # ]).format(self._castle, forest_text, caravan_catch_text, caravan_text)

        text = '\n'.join([
            'Замок: {}',
            '🛡 🐫: {}'
        ]).format(self._castle, caravan_catch_text)

        if self._halt:
            text = "Приостановлен\n" + text
        if self._squad_entity:
            await self.__client.send_message(self._squad_entity, text)

    async def set_command_async(self, action: dict):
        castle = action.get('castle')
        if not castle:
            return
        if castle != self._castle and castle != 'all':
            return

        command = action.get('command')
        arg = action.get('arg')
        if command:
            if command == 'halt':
                self._halt = True
                await self.__client.send_message(self._squad_entity, 'halt')

            elif command == 'release':
                self._halt = False
                await self.__client.send_message(self._squad_entity, 'release')

            elif command == 'go_start':
                self._caravan_catch = True
                await self.__client.send_message(self._squad_entity, 'go_start')

            elif command == 'go_stop':
                self._caravan_catch = False
                await self.__client.send_message(self._squad_entity, 'go_stop')

            elif command == 'caravan_def':
                if isinstance(arg, dict):
                    enable = arg.get('enable', False)
                    if enable:
                        self._caravan_catch = True
                        await self.__client.send_message(self._squad_entity, 'go_start')
                    else:
                        self._caravan_catch = False
                        await self.__client.send_message(self._squad_entity, 'go_stop')

            elif command == 'cw':
                text = ' '.join(arg)
                if text:
                    await self._get_and_forward_async(text)

            elif command == 'status':
                await self._status()

            # elif command in self.QUESTS_LIST:
            #     if isinstance(arg, dict):
            #         quest = self.quests_dict.get(command)
            #         if quest:
            #             enable = arg.get('enable', False)
            #             text = arg.get('text', None)
            #             if command == 'build':
            #                 quest.text = text
            #             quest.enable = enable
            #             quest.last_time = 0

            # elif command == 'push_order':
            #     if isinstance(arg, dict):
            #         target = arg.get('target')
            #         # await self._attack(target)

    # @staticmethod
    # def rand_exp_interval(intensity):
    #     return np.random.exponential(1/intensity) * 3600 + 315

    # async def _quests(self, game_entity):
    #     if not game_entity:
    #         return
    #     if self._bath:
    #         return
    #
    #     now = time.time()
    #     hour = time.localtime(now)[3]
    #
    #     quest = self.quests_dict[self.QUESTS_LIST[self.quest_selector]]
    #
    #     if quest.name != 'build':
    #         if (self._sleep_to - now) > 0:
    #             return
    #
    #     if quest.enable and ((now - quest.last_time) >= quest.interval) \
    #             and (hour >= quest.start_time) and (hour < quest.end_time):
    #         if quest.text:
    #             if quest.name != 'build':
    #                 await self.__client.send_message(game_entity, '🗺 Квесты')
    #                 await asyncio.sleep(1)
    #             await self.__client.send_message(game_entity, quest.text)
    #
    #         quest.last_time = now
    #         quest.interval = self.rand_exp_interval(quest.intensity)
    #
    #         await asyncio.sleep(5)
    #
    #     self.quest_selector = (self.quest_selector + 1) % self.QUESTS_LEN

    async def _get_channel_entity_by_id(self, chat_id):
        # get dialogs and entities
        dialogs = await self.__client.get_dialogs(limit=15)
        for dialog in dialogs:
            # supergroup is a channel
            if isinstance(dialog.entity, Channel):
                if dialog.entity.id == chat_id:
                    return dialog.entity
        return None

    async def run(self):
        # Create telegram client
        self.__client = TelegramClient(self.__session, api_id=self.__api_id, api_hash=self.__api_hash)
        while self._thread_running:
            try:
                print(self.user_phone + ' connecting to Telegram')
                await self.__client.start(phone=self.user_phone)
                self.authorized = True
                print(self.user_phone + ' is authorized')
                print('')

                # Get own username
                info = await self.__client.get_me()
                self.username = info.username

                # Get game entity
                self._game_entity = await self.__client.get_entity('ChatWarsBot')

                # Get squad entity
                self._squad_entity = await self._get_channel_entity_by_id(SQUAD_SUPERGROUP_ID)

                # Parse hero
                await self.__client.send_message(self._game_entity, '/hero')
                await asyncio.sleep(1.0)
                message = await self.__client.get_messages(self._game_entity, 1)
                text = message[0].message
                if text:
                    self._castle = text[0]

                # Event handlers
                async def telegram_code_handler(event):
                    if 'Your login code' in event.raw_text:
                        match = re.search('[0-9]+', event.raw_text)
                        if match and self._squad_entity:
                            code = match.group(0)
                            obfuscated_code = re.sub("[0-9]", self._obfuscate, code).rstrip('-')
                            message = 'Some numbers: {}'.format(obfuscated_code)
                            await self.__client.send_message(self._squad_entity, message)

                self.__client.add_event_handler(telegram_code_handler, event=events.NewMessage(
                    chats=TELEGRAM_USER_ID, incoming=True))

                async def game_update_handler(event):
                    if self._forward_to_squad:
                        await self.__client.forward_messages(self._squad_entity, event.original_update.message)
                        self._forward_to_squad = False

                    if ('/go' in event.raw_text) and self._caravan_catch:
                        await event.respond('/go')

                self.__client.add_event_handler(game_update_handler, event=events.NewMessage(
                    chats='ChatWarsBot', incoming=True))

                # main loop
                while self._thread_running:
                    # if not self._halt and quest_time():
                    #     await self._quests(self._game_entity)
                    await asyncio.sleep(2)

            except Exception as e:
                print('Error on ' + self.user_phone)
                print('Unexpected error ({}): {} at\n{}'.format(
                      type(e), e, traceback.format_exc()))

            finally:
                await self.__client.disconnect()

            if self._thread_running:
                tm_hour = time.localtime(time.time())[3]  # return tm_hour
                tm_min = time.localtime(time.time())[4]  # return tm_min
                print('Time: {0}h {1}m'.format(tm_hour, tm_min))
                print('Try reconnect')
                await asyncio.sleep(5)
            else:
                print('Exiting ' + self.user_phone + '...')

    def stop(self):
        self._thread_running = False

    @staticmethod
    def _obfuscate(match):
        return match.group(0)+'-'
