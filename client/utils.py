from datetime import datetime

import pytz
from pytz import timezone


def get_ru_dt():
    utc = pytz.utc
    utc_dt = utc.localize(datetime.utcnow())
    ru_tz = timezone('Europe/Moscow')
    ru_dt = ru_tz.normalize(utc_dt.astimezone(ru_tz))
    return ru_dt


def quest_time():
    dt = get_ru_dt()
    cond1 = (dt.hour % 4 == 3) and (dt.minute > 50)
    cond2 = (dt.hour % 4 == 0) and (dt.minute < 5)
    if not(cond1 or cond2):
        return True
    return False


def battle_time():
    dt = get_ru_dt()
    if (dt.hour % 4 == 3) and (dt.minute >= 57):
        return True
    return False
