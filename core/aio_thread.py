import asyncio
from threading import Thread, Event


class AioThread(Thread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.loop, self.event = None, Event()

    def run(self):
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.loop.call_soon(self.event.set)
        self.loop.run_forever()

    def add_task(self, coroutine, argument):
        if argument:
            asyncio.run_coroutine_threadsafe(coroutine(argument), loop=self.loop)
        else:
            asyncio.run_coroutine_threadsafe(coroutine(), loop=self.loop)

    def finalize(self):
        self.loop.call_soon_threadsafe(self.loop.stop)
        self.join()
