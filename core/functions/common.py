import json
import logging

from telegram import Update, Bot


from core.functions.reply_markup import generate_admin_markup
from core.functions.inline_keyboard_handling import (generate_build_repair_buttons, generate_castle_buttons,
                                                     generate_quests_buttons, generate_attack_buttons)
from core.texts import*
from core.utils import send_async
import configparser

config = configparser.ConfigParser()
config.read('config.ini')
ADMINS = json.loads(config['CORE']['ADMINS'])

LOGGER = logging.getLogger(__name__)
MSG_PING = 'pong, @{}'


def admin_allowed(func):
    def wrapper(bot: Bot, update: Update, *args, **kwargs):
        if update.message.from_user.username in ADMINS:
            func(bot, update, *args, **kwargs)
    return wrapper


def ping(bot: Bot, update: Update):
    send_async(bot, chat_id=update.message.chat.id, text=MSG_PING.format(update.message.from_user.username))


def start(bot: Bot, update: Update):
    send_async(bot, chat_id=update.message.chat_id, text="I'm bot, please talk to me!")


@admin_allowed
def admin_panel(bot: Bot, update: Update):
    if update.message.chat.type == 'private':
        send_async(bot, chat_id=update.message.chat.id, text=MSG_ADMIN_WELCOME,
                   reply_markup=generate_admin_markup())


def quests_show(bot: Bot, update: Update):
    if update.message.chat.type == 'private':
        text = 'Выбери замок'
        btns = generate_castle_buttons('Quest')
        send_async(bot, chat_id=update.message.chat_id, text=text, reply_markup=btns)


def build_show(bot: Bot, update: Update):
    if update.message.chat.type == 'private':
        text = 'Выбери замок'
        btns = generate_castle_buttons('Build')
        send_async(bot, chat_id=update.message.chat_id, text=text, reply_markup=btns)


def attack_show(bot: Bot, update: Update):
    if update.message.chat.type == 'private':
        text = 'Выбери замок'
        btns = generate_castle_buttons('Attack')
        send_async(bot, chat_id=update.message.chat_id, text=text, reply_markup=btns)
