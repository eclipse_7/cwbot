import json
from enum import Enum

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Bot, Update

CASTLES = ('🍆', '🖤', '🦇', '🐢', '🍁', '☘', '🌹')

class QueryType(Enum):
    Quest = 1
    Build = 2
    Repair = 3
    BuildTarget = 4
    Attack = 5
    Castle = 6

    On = 15
    Off = 16

    BuildOff = 19
    Interval = 20


def generate_on_off_buttons():
    inline_keys = [[InlineKeyboardButton('On', callback_data=json.dumps(
                        {'t': QueryType.On.value}))],
                   [InlineKeyboardButton('Off', callback_data=json.dumps(
                        {'t': QueryType.Off.value}))]]
    return InlineKeyboardMarkup(inline_keys)


def generate_build_repair_buttons():
    inline_keys = [[InlineKeyboardButton('Build', callback_data=json.dumps(
                        {'t': QueryType.Build.value}))],
                   [InlineKeyboardButton('Repair', callback_data=json.dumps(
                        {'t': QueryType.Repair.value}))],
                   [InlineKeyboardButton('Off', callback_data=json.dumps(
                        {'t': QueryType.BuildOff.value}))]]
    return InlineKeyboardMarkup(inline_keys)


# def generate_quests_buttons():
#     inline_keys = [[InlineKeyboardButton('🌲Лес', callback_data=json.dumps(
#                         {'t': QueryType.Quest.value, 'target': 'forest'}))],
#                    [InlineKeyboardButton('🛡 🐫', callback_data=json.dumps(
#                        {'t': QueryType.Quest.value, 'target': 'caravan_def'}))],
#                    [InlineKeyboardButton('🗡 🐫', callback_data=json.dumps(
#                        {'t': QueryType.Quest.value, 'target': 'caravan'}))]]
#     return InlineKeyboardMarkup(inline_keys)


def generate_quests_buttons():
    inline_keys = [
                   [InlineKeyboardButton('🛡 🐫', callback_data=json.dumps(
                       {'t': QueryType.Quest.value, 'target': 'caravan_def'}))]
                   ]
    return InlineKeyboardMarkup(inline_keys)


def generate_castle_buttons(mode):
    inline_keys = [[InlineKeyboardButton(CASTLES[0], callback_data=json.dumps(
                        {'t': QueryType.Castle.value, 'castle': CASTLES[0], 'mode': mode})),
                    InlineKeyboardButton(CASTLES[1], callback_data=json.dumps(
                        {'t': QueryType.Castle.value, 'castle': CASTLES[1], 'mode': mode}))],
                   [InlineKeyboardButton(CASTLES[2], callback_data=json.dumps(
                       {'t': QueryType.Castle.value, 'castle': CASTLES[2], 'mode': mode})),
                    InlineKeyboardButton(CASTLES[3], callback_data=json.dumps(
                        {'t': QueryType.Castle.value, 'castle': CASTLES[3], 'mode': mode}))],
                   [InlineKeyboardButton(CASTLES[4], callback_data=json.dumps(
                       {'t': QueryType.Castle.value, 'castle': CASTLES[4], 'mode': mode})),
                    InlineKeyboardButton(CASTLES[5], callback_data=json.dumps(
                        {'t': QueryType.Castle.value, 'castle': CASTLES[5], 'mode': mode}))],
                   [InlineKeyboardButton(CASTLES[6], callback_data=json.dumps(
                       {'t': QueryType.Castle.value, 'castle': CASTLES[6], 'mode': mode})),
                    InlineKeyboardButton('All', callback_data=json.dumps(
                        {'t': QueryType.Castle.value, 'castle': 'all', 'mode': mode}))]]
    return InlineKeyboardMarkup(inline_keys)


def generate_attack_buttons():
    inline_keys = [[InlineKeyboardButton(CASTLES[0], callback_data=json.dumps(
                        {'t': QueryType.Attack.value, 'target': CASTLES[0]})),
                    InlineKeyboardButton(CASTLES[1], callback_data=json.dumps(
                        {'t': QueryType.Attack.value, 'target': CASTLES[1]}))],
                   [InlineKeyboardButton(CASTLES[2], callback_data=json.dumps(
                       {'t': QueryType.Attack.value, 'target': CASTLES[2]})),
                    InlineKeyboardButton(CASTLES[3], callback_data=json.dumps(
                        {'t': QueryType.Attack.value, 'target': CASTLES[3]}))],
                   [InlineKeyboardButton(CASTLES[4], callback_data=json.dumps(
                       {'t': QueryType.Attack.value, 'target': CASTLES[4]})),
                    InlineKeyboardButton(CASTLES[5], callback_data=json.dumps(
                        {'t': QueryType.Attack.value, 'target': CASTLES[5]}))],
                   [InlineKeyboardButton(CASTLES[6], callback_data=json.dumps(
                       {'t': QueryType.Attack.value, 'target': CASTLES[6]}))]]
    return InlineKeyboardMarkup(inline_keys)


def generate_build_target_buttons():
    inline_keys = [[InlineKeyboardButton('wall', callback_data=json.dumps(
                        {'t': QueryType.BuildTarget.value, 'target': 'wall'})),
                    InlineKeyboardButton('ambar', callback_data=json.dumps(
                        {'t': QueryType.BuildTarget.value, 'target': 'ambar'}))],
                   [InlineKeyboardButton('stash', callback_data=json.dumps(
                       {'t': QueryType.BuildTarget.value, 'target': 'stash'})),
                    InlineKeyboardButton('hunters', callback_data=json.dumps(
                        {'t': QueryType.BuildTarget.value, 'target': 'hunters'}))],
                   [InlineKeyboardButton('hq', callback_data=json.dumps(
                       {'t': QueryType.BuildTarget.value, 'target': 'hq'})),
                    InlineKeyboardButton('gladiators', callback_data=json.dumps(
                        {'t': QueryType.BuildTarget.value, 'target': 'gladiators'}))],
                   [InlineKeyboardButton('warriors', callback_data=json.dumps(
                       {'t': QueryType.BuildTarget.value, 'target': 'warriors'})),
                    InlineKeyboardButton('teaparty', callback_data=json.dumps(
                        {'t': QueryType.BuildTarget.value, 'target': 'teaparty'}))],
                   [InlineKeyboardButton('monument', callback_data=json.dumps(
                       {'t': QueryType.BuildTarget.value, 'target': 'monument'})),
                    InlineKeyboardButton('saboteurs', callback_data=json.dumps(
                        {'t': QueryType.BuildTarget.value, 'target': 'saboteurs'}))],
                   [InlineKeyboardButton('sentries', callback_data=json.dumps(
                       {'t': QueryType.BuildTarget.value, 'target': 'sentries'}))]]
    return InlineKeyboardMarkup(inline_keys)


def callback(bot: Bot, update: Update, clients, aiothread, chat_data: dict):
    data = json.loads(update.callback_query.data)
    # Select castle
    if data['t'] == QueryType.Castle.value:
        chat_data['castle'] = data['castle']
        mode = data['mode']
        msg = None
        inline_keys = None
        if mode == 'Quest':
            msg = chat_data['castle']
            inline_keys = generate_quests_buttons()
        elif mode == 'Build':
            msg = chat_data['castle']
            inline_keys = generate_build_repair_buttons()
        elif mode == 'Attack':
            msg = chat_data['castle'] + ' ' + '->  '
            inline_keys = generate_attack_buttons()
        if msg and inline_keys:
            bot.editMessageText(msg, update.callback_query.message.chat.id, update.callback_query.message.message_id,
                                reply_markup=inline_keys)

    if data['t'] == QueryType.On.value:
        chat_data['status'] = 'on'
        msg = chat_data['castle'] + ' ' + chat_data['type'] + ' ' + chat_data['target'] + ' ' + chat_data['status']
        bot.editMessageText(msg, update.callback_query.message.chat.id, update.callback_query.message.message_id)
        send_command(clients, aiothread, chat_data)

    if data['t'] == QueryType.Off.value:
        chat_data['status'] = 'off'
        msg = chat_data['castle'] + ' ' + chat_data['type'] + ' ' + chat_data['target'] + ' ' + chat_data['status']
        bot.editMessageText(msg, update.callback_query.message.chat.id, update.callback_query.message.message_id)
        send_command(clients, aiothread, chat_data)

    if data['t'] == QueryType.Quest.value:
        chat_data['type'] = 'quest'
        chat_data['target'] = data['target']
        msg = chat_data['castle'] + ' ' + chat_data['type'] + ' ' + chat_data['target']
        inline_keys = generate_on_off_buttons()
        bot.editMessageText(msg, update.callback_query.message.chat.id, update.callback_query.message.message_id,
                            reply_markup=inline_keys)

    if data['t'] == QueryType.Build.value:
        chat_data['type'] = 'build'
        chat_data['status'] = 'on'
        msg = chat_data['castle'] + ' ' + chat_data['type']
        inline_keys = generate_build_target_buttons()
        bot.editMessageText(msg, update.callback_query.message.chat.id, update.callback_query.message.message_id,
                            reply_markup=inline_keys)

    if data['t'] == QueryType.Repair.value:
        chat_data['type'] = 'repair'
        chat_data['status'] = 'on'
        msg = chat_data['castle'] + ' ' + chat_data['type']
        inline_keys = generate_build_target_buttons()
        bot.editMessageText(msg, update.callback_query.message.chat.id, update.callback_query.message.message_id,
                            reply_markup=inline_keys)

    if data['t'] == QueryType.BuildOff.value:
        chat_data['type'] = 'build'
        chat_data['status'] = 'off'
        msg = chat_data['castle'] + ' ' + chat_data['type'] + ' ' + chat_data['status']
        bot.editMessageText(msg, update.callback_query.message.chat.id, update.callback_query.message.message_id)
        send_command(clients, aiothread, chat_data)

    if data['t'] == QueryType.BuildTarget.value:
        chat_data['target'] = data['target']
        msg = chat_data['castle'] + ' ' + chat_data['type'] + ' ' + chat_data['target'] + ' ' + chat_data['status']
        bot.editMessageText(msg, update.callback_query.message.chat.id, update.callback_query.message.message_id)
        send_command(clients, aiothread, chat_data)

    if data['t'] == QueryType.Attack.value:
        chat_data['type'] = 'attack'
        chat_data['target'] = data['target']
        msg = chat_data['castle'] + ' ' + '-> ' + chat_data['target']
        bot.editMessageText(msg, update.callback_query.message.chat.id, update.callback_query.message.message_id)
        send_command(clients, aiothread, chat_data)


def send_command(clients, aiothread, data: dict):
    if data['type'] == 'quest':
        if data['status'] == 'on':
            request = {'command': data['target'],
                       'castle': data['castle'],
                       'arg': {'enable': True}
                       }
        else:
            request = {'command': data['target'],
                       'castle': data['castle'],
                       'arg': {'enable': False}
                       }
        for client in clients:
            aiothread.add_task(client.set_command_async, request)

    elif (data['type'] == 'build') or (data['type'] == 'repair'):
        if data['status'] == 'on':
            request = {'command': 'build',
                       'castle': data['castle'],
                       'arg': {'enable': True,
                               'text': '/'+data['type']+'_'+data['target']}
                       }
        else:
            request = {'command': 'build',
                       'castle': data['castle'],
                       'arg': {'enable': False}
                       }
        for client in clients:
            aiothread.add_task(client.set_command_async, request)
    
    elif data['type'] == 'attack':
        request = {'command': 'push_order',
                   'castle': data['castle'],
                   'arg': {'target': data['target']}}
        for client in clients:
            aiothread.add_task(client.set_command_async, request)
