from telegram import ReplyKeyboardMarkup, KeyboardButton

from core.commands import *


def generate_admin_markup():
    buttons = [[KeyboardButton(ADMIN_COMMAND_QUESTS),
                KeyboardButton(ADMIN_COMMAND_ATTACK)]]
    return ReplyKeyboardMarkup(buttons, True)
