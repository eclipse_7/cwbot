import configparser
import time

from telegram import (
    Bot, Update
)
from telegram.ext import (
    Updater, CommandHandler, MessageHandler,
    Filters, CallbackQueryHandler
)
from telegram.ext.dispatcher import run_async

from client.cw_bot import CWGameBot
from core.aio_thread import AioThread
from core.commands import *
from core.functions.common import ping, start, admin_panel, quests_show, build_show, attack_show, admin_allowed
from core.functions.inline_keyboard_handling import callback

# import logging
# logging.basicConfig(level=logging.DEBUG)
# logging.getLogger('telethon')

config = configparser.ConfigParser()
config.read('config.ini')

TOKEN = config['BOT']['TOKEN']
APP_API_ID = int(config['APP']['API_ID'])
APP_API_HASH = config['APP']['API_HASh']

COMMANDS_LIST = ('halt', 'release', 'go_start', 'go_stop', 'status', 'cw')

# list of clients
clients = []

# Start asyncioThread
aiothread = AioThread()
aiothread.start()
aiothread.event.wait()


def set_command_for_all_clients(command):
    for client in clients:
        aiothread.add_task(client.set_command_async, command)


def set_command(client: CWGameBot, command):
    aiothread.add_task(client.set_command_async, command)


@run_async
def callback_query(bot: Bot, update: Update, chat_data: dict):
    callback(bot, update, clients, aiothread, chat_data)


@run_async
@admin_allowed
def manage_all(bot: Bot, update: Update):
    if update.message.chat.type in ('group', 'supergroup'):
        if update.message.text:
            text = update.message.text
            params = text.split(' ')
            username = params[0]
            command = params[1]
            arg = params[2:]

            if command not in COMMANDS_LIST:
                return

            # if trade, add from_user_username to arg[]
            from_username = update.message.from_user.username
            if command == 'trade' and from_username:
                arg = [from_username] + arg

            request = {'username': username,
                       'castle': 'all',
                       'command': command,
                       'arg': arg}
            if username and command:
                if username == 'all' and command != 'market':
                    set_command_for_all_clients(request)
                else:
                    for client in clients:
                        if client.get_username() == username:
                            set_command(client, request)
                            break

    elif update.message.chat.type == 'private':
        if update.message.text:
            text = update.message.text
            if text == ADMIN_COMMAND_QUESTS:
                quests_show(bot, update)

            elif text == ADMIN_COMMAND_BUILD:
                build_show(bot, update)
            
            elif text == ADMIN_COMMAND_ATTACK:
                attack_show(bot, update)

            else:
                params = text.split(' ')
                username = params[0]
                command = params[1]
                arg = params[2:]

                if command not in COMMANDS_LIST:
                    return

                # if trade, add from_user_username to arg[]
                from_username = update.message.from_user.username
                if command == 'trade' and from_username:
                    arg = [from_username] + arg

                request = {'username': username,
                           'castle': 'all',
                           'command': command,
                           'arg': arg}
                if username and command:
                    if username == 'all' and command != 'market':
                        set_command_for_all_clients(request)
                    else:
                        for client in clients:
                            if client.get_username() == username:
                                set_command(client, request)
                                break


def main():
    # Load the settings and initialize the client
    path_to_sessions = 'sessions/'
    path_to_users = 'phones/settings'

    with open(path_to_users, 'r', encoding='utf-8') as file:
        for line in file:
            value_pairs = line.strip().split(' ')
            # print(value_pairs)
            result = {}
            for item in value_pairs:
                if item == '':
                    continue
                values = item.split('=')
                left = values[0].strip()
                right = values[1].strip()
                if right.isnumeric():
                    result[left] = int(right)
                else:
                    result[left] = right

            settings = result
            if not(settings.get('user_phone') and settings.get('session_name')):
                continue

            client = CWGameBot(
                session_name=str(path_to_sessions + settings.get('session_name', 'anonymous')),
                user_phone=str(settings['user_phone']),
                api_id=APP_API_ID,
                api_hash=APP_API_HASH)

            # client start
            aiothread.add_task(client.run, None)
            while not client.authorized:
                time.sleep(0.1)
            clients.append(client)

    # Create the EventHandler and pass it your bot's token.
    updater = Updater(TOKEN)

    # Get the dispatcher to register handlers
    disp = updater.dispatcher

    # on different commands - answer in Telegram
    disp.add_handler(CommandHandler('start', start))
    disp.add_handler(CommandHandler('admin', admin_panel))
    disp.add_handler(CommandHandler('ping', ping))
    disp.add_handler(CallbackQueryHandler(callback_query, pass_chat_data=True))
    disp.add_handler(MessageHandler(Filters.text, manage_all))

    # Start the Bot
    updater.start_polling(poll_interval=1)

    while True:
        s = input("Press Enter to exit")
        if s == '':
            break

    updater.stop()
    for client in clients:
        client.stop()
    aiothread.finalize()


if __name__ == '__main__':
    main()
